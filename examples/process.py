#!/usr/bin/env python

from minionize import ProcessCallback, minionizer

# user defined
class MyCallback(ProcessCallback):
    def to_cmd(self, param):
        return f"echo {param}"


callback = MyCallback()

minionize = minionizer(callback)
minionize.run()
